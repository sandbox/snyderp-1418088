Smartqueue Views
===

About
---

Note: This module does not provide the ability to display smartqueues through
views.  That functionality already exists in the core Nodequeue module.  This
module allows for creating and limiting subqueues based on views.

A module to allow for creating subqueues for nodes specified by a view.  The
module further, optionally allows for further filtering the nodes that can
be assigned to each of these subqueues with a further view, which is provided
with the nid of node that owns a given subqueue.

Usage
---

After enabling the module, create a view ("view a") that returns a field of
nids.  Then create a new subqueue by clicking on the "add view generated queue"
tab in the nodequeue admin panel.  Select your newly created view in the "View
for deciding which nodes have subqueues" field.

Optionally, if you want to limit the nodes that can be added to these subqueues
beyond by content type, of you can create a second view ("view b").  This second
view should return a set of nids.  This second view will be provided a nid as an
argument, which can also be used to further limit the results.  When editing the
subqueue, set this second view in the "View for which nodes can be added to
these subqueues" field.

Now, whenever you view a node returned by "view b", you'll see the "nodequeue",
tab, indicating that this node can be added to one of the subqueues that belong
to one of the nodes returned by "view a."

 - snyderp <snyderp@gmail.com>